from robusta.api import *
import json

class WhjrEventConfig(ActionParams):
    pass

@action
def whjr_event_enricher(event: KubernetesAnyChangeEvent, config: WhjrEventConfig):
    if not event.obj.metadata:  # shouldn't happen, just to be on the safe side
        logging.warning(f"resource_babysitter skipping resource with no meta - {event.obj}")
        return
    # logging.info(f"Event Received of component :{params.name} and namespace :{params.namespace} and value is {params.rate_limit}")
    logging.info(event.obj.to_dict())