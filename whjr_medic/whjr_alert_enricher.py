from robusta.api import *
import logging
import datetime


class DeploymentParams(RateLimitParams):
    name: str
    namespace: str


@action
def deployment_restart(alert: PrometheusKubernetesAlert, params: DeploymentParams):
    logging.info(f"Triggered the prom alert object: {alert} and param object :{params}")
    if not RateLimiter.mark_and_test(
            "deployment_restart", params.name + params.namespace, params.rate_limit
    ):
        logging.info(
            f"Returning and ignoring execution due to rate limit definition for deployment :{params.name} and namespace :{params.namespace} and value is {params.rate_limit}")
        return
    now = datetime.datetime.utcnow()
    now = str(now.isoformat("T") + "Z")
    try:
        my_deployment: RobustaDeployment = RobustaDeployment.readNamespacedDeployment(params.name, params.namespace).obj
        my_deployment.spec.template.metadata.annotations['kubectl.kubernetes.io/restartedAt'] = now
        my_deployment.spec.template.metadata.annotations['restartedAt'] = now
        logging.info(f"Updated the deployment for restart for alert: {alert}")
        my_deployment.update()
    except:
        logging.exception(f"Failed to update deployment for alert")


@action
def pod_delete(alert: PrometheusKubernetesAlert):
    logging.info(f"Triggered the prom alert object: {alert}")

    pod_name = alert['labels']['pod']
    namespace = alert['labels']['namespace']
    logging.info(f"Alert created for pod {pod_name} and namespace {namespace}")
    # if namespace != params.namespace:
    #     logging.info(
    #         f"Returning and ignoring execution due to definition of namespace restriction")
    #     return
    my_pod: RobustaPod = RobustaPod.read(pod_name, namespace)
    logging.info(f"Fetched object is {my_pod}")

    # Deleting the pod
    #RobustaPod.deleteNamespacedPod(pod_name, namespace)


# this runs on Prometheus alerts you specify in the YAML
@action
def whjr_alert_enricher(event: PrometheusKubernetesAlert):
    # we have full access to the pod on which the alert fired
    pod = event.get_pod()
    pod_name = pod.metadata.name
    pod_logs = pod.get_logs()
    pod_processes = pod.exec("ps aux")

    logging.info(f"Debugging the prom alert object: {event}")

    # this is how you send data to slack or other destinations
    event.add_enrichment([
        MarkdownBlock("*Oh no!* An alert occurred on " + pod_name),
        FileBlock("crashing-pod.log", pod_logs)
    ])
